/**
 * bai 1
 */
function hasEqualToZero(firstScore, secondScore, thirdScore) {
    if (firstScore == 0 || secondScore == 0 || thirdScore == 0)
        return true;
    return false;
}

function getAreaScore(area) {
    switch (area) {
        case 'A':
            return 2;
        case 'B':
            return 1;
        case 'C':
            return .5;
        default:
            return 0;
    }
}

function getSpecificObjScore(specificObject) {
    switch (specificObject) {
        case 1:
            return 2.5;
        case 2:
            return 1.5;
        case 3:
            return 1;
        default:
            return 0;
    }
}

function handleAdmissions() {
    var standardScore = document.getElementById("standardScore").value * 1;
    var firstScore = document.getElementById("firstScore").value * 1;
    var secondScore = document.getElementById("secondScore").value * 1;
    var thirdScore = document.getElementById("thirdScore").value * 1;
    var area = document.getElementById("area").value;
    var specificObject = document.getElementById("specificObject").value * 1;
    var priorityScore = getAreaScore(area) + getSpecificObjScore(specificObject);
    var finalScore = firstScore + secondScore + thirdScore + priorityScore;
    var result = "";
    if (!hasEqualToZero(firstScore, secondScore, thirdScore) && finalScore >= standardScore) {
        result = `
            <p class="alert-success">Chúc mừng bạn đã đậu!</p>
            <p class="alert-success">Số điểm bạn đạt được: ${finalScore}</p>
        `
    } else {
        result = `
            <p class="alert-danger">Rất tiếc, bạn đã trượt!</p>
            <p class="alert-danger">Số điểm bạn đạt được: ${finalScore}</p>
        `
    }

    document.getElementById("result").innerHTML = result;
}

// bai2
function getfirst50KmFeeByKw(kw) {
    if (kw <= 50) {
        return 500;
    }
}

function handleElectricityBill() {
    var name = document.getElementById("username").value;
    var kw = document.getElementById("numberOfKw").value * 1;
    var fee = 0;
    if (kw <= 50) {
        fee = kw * 500;
    } else if (kw <= 100) {
        fee = 50 * 500 + (kw - 50) * 650;
    } else if (kw <= 200) {
        fee = 50 * 500 + 50 * 650 + (kw - 100) * 850;
    } else if (kw <= 350) {
        fee = 50 * 500 + 50 * 650 + 100 * 850 + (kw - 200) * 1100;
    } else {
        fee = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kw - 350) * 1300;
    }
    document.getElementById("result_2").innerHTML = `
        <p>${name} - ${fee.toLocaleString()}</p>
    `;
}

//bai3
function calculateIncomeTax() {
    var fullName = document.getElementById("fullName").value;
    var generalIncome = document.getElementById("generalIncome").value * 1;
    var numberOfPeople = document.getElementById("numberOfPeople").value * 1;
    var tax = getCalculateTax(generalIncome, numberOfPeople);
    console.log(tax)
    var finalTax = getFinalTax(tax);
    console.log(finalTax)
    document.getElementById("result_3").innerHTML = `
        <p>${fullName} - ${finalTax.toLocaleString()}</p>
    `
}

function getCalculateTax(generalIncome, numberOfPeople) {
    return generalIncome - 4000000 - numberOfPeople * 1600000;
}

function getFinalTax(tax) {
    if (tax <= 60e6) {
        return tax * .05;
    } else if (tax <= 120e6) {
        return tax * .1;
    } else if (tax <= 210e6) {
        return tax * .15;
    } else if (tax <= 384e6) {
        return tax * .2;
    } else if (tax <= 624e6) {
        return tax * .25;
    } else if (tax <= 960e6) {
        return tax * .3;
    } else {
        return tax * .35
    }
}

//bai4
var isntChanged = true;
function calculated() {
    console.log(isntChanged)
    var selected = document.getElementById("selected").value;
    var guestID = document.getElementById("guestID").value;
    var connectNumber;
    if (isntChanged) {
        connectNumber = 0;
    } else {
        connectNumber = document.getElementById("connectNumber").value * 1;
    }
    console.log("🚀 ~ file: app.js:130 ~ calculated ~ connectNumber:", connectNumber)
    var highPortNumber = document.getElementById("highPortNumber").value * 1;
    console.log("🚀 ~ file: app.js:128 ~ calculated ~ selected:", selected)
    var finalFee = getHandleFee(selected) + getStandardFee(selected, connectNumber) + highPortNumber * getHighPorFee(selected);
    document.getElementById("result_4").innerHTML = `
        <p>GuestID: ${guestID} - Fee: $${finalFee.toLocaleString('en-US')}</p>
    `
}

function getHandleFee(selected) {
    switch (selected) {
        case 'normal':
            return 4.5;
        case 'business':
            return 15;
        default:
            return;
    }
}

function getStandardFee(selected, connectNumber) {
    switch (selected) {
        case 'normal':
            return 20.5;
        case 'business':
            if (connectNumber <= 10) {
                return 75;
            } else {
                return 75 + (connectNumber - 10) * 5;
            }
        default:
            return;
    }
}

function getHighPorFee(selected) {
    switch (selected) {
        case 'normal':
            return 7.5;
        case 'business':
            return 50;
        default:
            return;
    }
}

function handleSubSelect() {
    isntChanged = false;
    var selected = document.getElementById("selected").value;
    if (selected == 'business') {
        document.getElementById("sub_select").innerHTML = `
            <div class="form-group mt-2">
                <label for="">Số kết nối:</label>
                <input type="text" class="form-control" name="" id="connectNumber" placeholder="Số kết nối">
            </div>
            `
    } else {
        document.getElementById("sub_select").innerHTML = "";
        isntChanged = true;
    }
}